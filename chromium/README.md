# chromium browser #

This docker image runs a chromium browser accessible using vnc

## Quick start ##
Clone the git repository build and run the container

~~~bash
git clone https://bitbucket.org/thwint/docker.git
cd chromium
docker build -t chromium .
docker-compose up -d
~~~


## Details ##
### Image ###
Based on a recent alpine linux image the image starts a X-server, a vnc server and a chrome browser.

#### Packages ####

* bash
* xvfb
* xdpyinfo
* x11vnc
* chromium

### docker-compose example ###

Sample configuration:

~~~yaml
version: '2.1'
services:
  chromium:
    image: chromium
    container_name: chromium
    restart: always
    hostname: chromium
    networks:
      - guacamole
    environment:
      BROWSER_URL: www.post.ch
    ports:
      - 5900:5900

networks:
  guacamole:
    name: guacamole
    external: true
~~~

